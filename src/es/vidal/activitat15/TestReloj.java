package es.vidal.activitat15;

public class TestReloj {
    public static void main(String[] args) {
        Reloj[] relojes = new Reloj[10];
        relojes[0] = new Reloj(40,18,20,"Rolex","T21",230);
        relojes[1] = new Reloj(23,10,30,"Mano","G233",54);
        relojes[2] = new Reloj(54,1,24,"Serio","H332",654);
        relojes[3] = new Reloj(24,0,13,"HxH","P344",123);
        relojes[4] = new Reloj(12,7,43,"Sasugu","Q654",34);
        relojes[5] = new Reloj(44,2,34,"Peir","L743",97);
        relojes[6] = new Reloj(32,22,23,"Sasulo","Ñ34",607);
        relojes[7] = new Reloj(11,20,33,"Teo","X63",703);
        relojes[8] = new Reloj(45,15,53,"Wireles","T89",76);
        relojes[9] = new Reloj(53,19,15,"Boiso","TYT32",973);
        ordenarPorMarca(relojes);
        mostrarReloj(relojes);

    }
    public static void ordenarPorMarca(Reloj[] relojes){
        for (int i = 0; i < relojes.length - 1; i++) {
            int indiceElementoMayor = i;
            for (int j = i + 1; j < relojes.length ; j++) {
                if (relojes[j].getMarca().compareTo(relojes[indiceElementoMayor].getMarca()) < 1){
                    indiceElementoMayor = j;
                }
            }
            if (i != indiceElementoMayor){
                intercambio(relojes, i, indiceElementoMayor);
            }
        }
    }

    public static void intercambio (Reloj[] relojes, int i, int indiceElementoMayor){
        Reloj aux = relojes[i];
        relojes[i] = relojes[indiceElementoMayor];
        relojes[indiceElementoMayor] = aux;
    }

    public static void mostrarReloj(Reloj[] relojes){
        for (int i = 0; i < relojes.length; i++) {
            System.out.printf("Marca: %s |Modelo: %s |Precio: %d |Hora: ", relojes[i].getMarca(), relojes[i].getModelo(), relojes[i].getPrecio());
            relojes[i].mostrarHora();
            System.out.println();
        }
    }
}
