package es.vidal.activitat15;

public class Reloj {

    enum Modo {H12, H24}

    private int minutos;

    private int horas;

    private int segundos;

    private String marca;

    private String modelo;

    private int precio;

    private Modo modoDeFuncionamiento;

    public Reloj(int segundos, int minutos, int horas){
        this.segundos = segundos;
        this.minutos = minutos;
        this.horas = horas;
        modoDeFuncionamiento = Modo.H24;

    }

    public Reloj(){
        segundos = 0;
        minutos = 0;
        horas = 0;
    }

    public Reloj(int minutos, int horas, int segundos, String marca, String modelo, int precio) {
        this.minutos = minutos;
        this.horas = horas;
        this.segundos = segundos;
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;
    }

    public int getMinutos() {
        return minutos;
    }

    public int getHoras() {
        return horas;
    }

    public int getSegundos() {
        return segundos;
    }

    public String getMarca() {
        return marca;
    }

    public String getModelo() {
        return modelo;
    }

    public int getPrecio() {
        return precio;
    }

    public Modo getModoDeFuncionamiento() {
        return modoDeFuncionamiento;
    }

    public void setHora(int minutos, int horas) {
        segundos = 0;
        this.minutos = minutos;
        this.horas = horas;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public void setHoraConSegundos(int segundos, int minutos, int horas) {
        this.segundos = segundos;
        this.minutos = minutos;
        this.horas = horas;
    }

    public void setModoDeFuncionamiento(Modo modoDeFuncionamiento) {
        this.modoDeFuncionamiento = modoDeFuncionamiento;
    }

    public void mostrarHora(){
        if (modoDeFuncionamiento == Modo.H12){
            mostrarHora12H();
        }else {
            mostrarHora24H();
        }
    }

    private void mostrarHora12H(){
        if (horas <= 12 && horas >= 0){
            System.out.printf("%d:%d:%dAM", horas, minutos, segundos);
        }else {
            System.out.printf("%d:%d:%dPM", horas-12, minutos, segundos);
        }
    }

    private void mostrarHora24H(){

        System.out.printf("%02d:%02d:%02d", horas, minutos, segundos);
    }
}
