package es.vidal.activitat16;

import java.time.LocalDate;
import java.time.Period;

public class Persona {

    private Fecha birthDay;

    private Address address;

    private String name;

    private String surname1;

    private String Surname2;

    private int telephoneNumber;

    private NIF nif;

    public Persona(NIF nif, String name, String surname1, String surname2, Fecha birthDay, Address address) {
        this.birthDay = birthDay;
        this.address = address;
        this.name = name;
        this.surname1 = surname1;
        Surname2 = surname2;
        this.nif = nif;
    }

    public boolean estaJubilado(){
        return calcularEdad(birthDay) >= 65;
    }

    public boolean esMayorDeEdad(){
        return calcularEdad(birthDay) >= 18;
    }

    public int calcularEdad(Fecha birthDay){
        Period periodo = Period.between(LocalDate.of(birthDay.getAnyo(), birthDay.getMes(), birthDay.getDia()), LocalDate.now());
        return periodo.getYears();
    }
}
