package es.vidal.activitat16;

public class Address {
    private String pais;

    private String provincia;

    private String ciudad;

    private int codigoPostal;

    public Address(String pais, String provincia, String ciudad, int codigoPostal) {
        this.pais = pais;
        this.provincia = provincia;
        this.ciudad = ciudad;
        this.codigoPostal = codigoPostal;
    }
}
