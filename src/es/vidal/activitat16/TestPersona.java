package es.vidal.activitat16;

public class TestPersona {
    public static void main(String[] args) {
        Persona jordi = generarPersona("22/02/2002", 21805945, "España",
        "Alicante", "Castalla", 03420, "Jordi", "Vidal","Cerdà");
        System.out.println(jordi.esMayorDeEdad());
        System.out.println(jordi.estaJubilado());

        Persona cris = generarPersona("24/04/1998", 21805963, "España",
                "Alicante", "Castalla", 03420, "Cristina", "Vidal","Cerdà");
        System.out.println(cris.esMayorDeEdad());
        System.out.println(cris.estaJubilado());

        Persona jose = generarPersona("14/06/1972", 21435654, "España",
                "Alicante", "Castalla", 03420, "José", "Vidal","Durà");
        System.out.println(jose.esMayorDeEdad());
        System.out.println(jose.estaJubilado());

        Persona cristina = generarPersona("28/08/1976", 21764321, "España",
                "Alicante", "Castalla", 03420, "Maria Cristina", "Cerdà","Belda");
        System.out.println(cristina.esMayorDeEdad());
        System.out.println(cristina.estaJubilado());

    }
    public static Persona generarPersona(String fechaNacimiento, int nif, String pais, String provincia, String ciudad, int codigoPostal, String name, String surname1, String surname2){
        Fecha fechaNacimientoPersona = new Fecha(fechaNacimiento);
        NIF nifPersona = new NIF(nif);
        Address addressPersona = new Address(pais, provincia, ciudad, codigoPostal);
        return new Persona(nifPersona, name, surname1, surname2, fechaNacimientoPersona, addressPersona);
    }
}
