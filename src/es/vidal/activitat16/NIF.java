//@Author: Jordi Vidal Cerdà//

package es.vidal.activitat16;

public class NIF {

    private final int DNI;

    private final char letra;

    public NIF(int DNI) {
        this.DNI = DNI;
        this.letra = getLetra(DNI);
    }

    public char getLetra(int dni){
        float ayuda = dni % 23;
        return switch ((int) ayuda) {
            case 0 -> 'T';
            case 1 -> 'R';
            case 2 -> 'W';
            case 3 -> 'A';
            case 4 -> 'G';
            case 5 -> 'M';
            case 6 -> 'Y';
            case 7 -> 'F';
            case 8 -> 'P';
            case 9 -> 'D';
            case 10 -> 'X';
            case 11 -> 'B';
            case 12 -> 'N';
            case 13 -> 'J';
            case 14 -> 'Z';
            case 15 -> 'S';
            case 16 -> 'Q';
            case 17 -> 'V';
            case 18 -> 'H';
            case 19 -> 'L';
            case 20 -> 'C';
            case 21 -> 'K';
            case 22 -> 'E';
            default -> ' ';
        };
    }

    public String getNIF() {
        String Dni = String.valueOf(this.DNI);
        String letter = String.valueOf(this.letra);
        return Dni+letter;
    }

    public int getDniNum(){
        return DNI;
    }

    public char getDni() {
        return letra;
    }
}
