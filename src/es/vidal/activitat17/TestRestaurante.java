package es.vidal.activitat17;

public class TestRestaurante {
    public static void main(String[] args) {
        Pizza[] pedidoPizzas1 = new Pizza[5];
        Pizza[] pedidoPizzas2 = new Pizza[5];
        Pizza[] pedidoPizzas3 = new Pizza[5];

        Ingrediente tomate = new Ingrediente("Tomate");
        Ingrediente mozzarella = new Ingrediente("Mozzarella");
        Ingrediente quesoAzul = new Ingrediente("Queso azul");
        Ingrediente quesoChedar = new Ingrediente("Queso chedar");
        Ingrediente quesoBase = new Ingrediente("Queso base");
        Ingrediente jamonYork = new Ingrediente("Jamón York");
        Masa masaFina = new Masa(Masa.Type.fina);
        Masa masaGruesa = new Masa(Masa.Type.gruesa);
        Masa masaBordeRellenoQueso = new Masa(Masa.Type.bordeRellenoQueso);

        pedidoPizzas1[0] = new Pizza("Prosciuto", masaFina, tomate, quesoBase);
        pedidoPizzas1[0].addExtraIngredient(jamonYork);
        pedidoPizzas1[1] = new Pizza("Margarita", masaGruesa, tomate, quesoBase, jamonYork, quesoChedar);

        pedidoPizzas2[0] = new Pizza("Prosciuto", masaFina, tomate, quesoBase);
        pedidoPizzas2[0].addExtraIngredient(jamonYork);
        pedidoPizzas2[1] = new Pizza("Margarita", masaGruesa, tomate, quesoBase, jamonYork, quesoChedar);
        pedidoPizzas2[2] = new Pizza("Devil", masaBordeRellenoQueso, tomate, quesoAzul, jamonYork, mozzarella);

        pedidoPizzas3[0] = new Pizza("Devil", masaBordeRellenoQueso, tomate, quesoAzul, jamonYork, mozzarella);
        pedidoPizzas3[0].addExtraIngredient(quesoChedar);

        showPrecioPedido(1, pedidoPizzas1);
        showPrecioPedido(2, pedidoPizzas2);
        showPrecioPedido(3, pedidoPizzas3);

    }
    public static void showPrecioPedido(int numPedido, Pizza[] pedido){
        System.out.printf("El precio del pedido %d es de %.2f€\n", numPedido, getPrecioPedido(pedido));
    }

    public static float getPrecioPedido(Pizza[] pedido){
        int precioTotal = 0;
        for (int i = 0; i < pedido.length ; i++) {
            if (pedido[i] != null)
            precioTotal += pedido[i].getPrice();
        }
        return precioTotal;
    }
}
