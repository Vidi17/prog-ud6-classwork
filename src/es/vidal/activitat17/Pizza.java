package es.vidal.activitat17;

public class Pizza {

    private final int MAX_INGREDIENTS = 5;

    private Ingrediente ingrediente;

    private Ingrediente[] ingredientes;

    private String name;

    private Masa masa;

    public void setName(String name) {
        this.name = name;
    }

    public void setMasa(Masa masa) {
        this.masa = masa;
    }

    public String getName() {
        return name;
    }

    public Masa getMasa() {
        return masa;
    }

    public Pizza(String name, Masa masa, Ingrediente... ingredientes) {
        this.ingredientes = new Ingrediente[MAX_INGREDIENTS];
        System.arraycopy(ingredientes, 0, this.ingredientes, 0, ingredientes.length);
        this.name = name;
        this.masa = masa;
    }

    public boolean addExtraIngredient(Ingrediente ingrediente){
        for (int i = 0; i < ingredientes.length; i++) {
            if (ingredientes[i] == null) {
                ingredientes[i] = ingrediente;
                return true;
            }
        }
        return false;
    }

    public float getPrice(){
        float price = masa.getPrice();
        for (Ingrediente ingrediente: ingredientes) {
            if (ingrediente != null) {
                price += ingrediente.getPrice();
            }else {
                return price;
            }
        }
        return price;
    }
}
