package es.vidal.activitat17;

public class Ingrediente {

    private String name;

    private float price;

    public Ingrediente(String name, float price) {
        this.name = name;
        this.price = price;
    }

    public Ingrediente(String name) {
        this.name = name;
        price = 1;
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }
}
