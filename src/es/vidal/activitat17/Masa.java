package es.vidal.activitat17;

public class Masa {

    enum Type {fina, gruesa, bordeRellenoQueso}

    private Type type;

    private float price;

    public Masa(Type type, float price) {
        this.type = type;
        this.price = price;
    }

    public Masa(Type type) {
        this.type = type;
        price = 5;
    }

    public Type getType() {
        return type;
    }

    public float getPrice() {
        return price;
    }
}
