package es.vidal.activitat18;

public class Tablero {

    public static final int NUMERO_0 = 0;

    public static final int NUMERO_3 = 3;

    private final EstadoCasilla[][] casillas = new EstadoCasilla[3][3];

    private final Icono packIconos;

    public Tablero(Icono packIconos) {
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                casillas[i][j] = EstadoCasilla.VACIA;
            }
        }
        this.packIconos = packIconos;
    }

    public void mostrar() {
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                System.out.printf("%s ", (packIconos.obtenerSimbolo(casillas[i][j])));
            }
            System.out.println();
        }
    }
    public boolean hayTresEnRaya() {
        for (int i = 0; i < casillas.length; i++) {
            if (contadorFichasFila(i)) {
                return true;
            }else if (contadorFichasColumna(i)) {
                return true;
            }
        }
        if (contadorFichasDiagonalMayor()) {
            return true;
        }else return contadorFichasSubdiagonal();
    }

    public boolean contadorFichasDiagonalMayor(){
        int contadorFichas0 = NUMERO_0;
        int contadorFichasX = NUMERO_0;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[i][i] == EstadoCasilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[i][i] == EstadoCasilla.FICHA_1){
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean contadorFichasSubdiagonal(){
        int contadorFichas0 = NUMERO_0;
        int contadorFichasX = NUMERO_0;
        int j = casillas.length - 1;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[i][j] == EstadoCasilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[i][j] == EstadoCasilla.FICHA_1){
                contadorFichasX++;
            }
            j--;
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean contadorFichasColumna(int columna){
        int contadorFichas0 = NUMERO_0;
        int contadorFichasX = NUMERO_0;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[i][columna] == EstadoCasilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[i][columna] == EstadoCasilla.FICHA_1){
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean contadorFichasFila(int fila){
        int contadorFichas0 = NUMERO_0;
        int contadorFichasX = NUMERO_0;
        for (int i = 0; i < casillas.length; i++) {
            if (casillas[fila][i] == EstadoCasilla.FICHA_0){
                contadorFichas0++;
            }else if (casillas[fila][i] == EstadoCasilla.FICHA_1){
                contadorFichasX++;
            }
        }
        return isTresEnRaya(contadorFichas0, contadorFichasX);
    }

    public boolean isTresEnRaya(int contadorFichas0, int contadorFichasX){
        return contadorFichas0 == NUMERO_3 || contadorFichasX == NUMERO_3;
    }

    public boolean isOcupada(Coordenada coordenada) {
        return casillas[coordenada.getX() - 1][coordenada.getY() - 1] == EstadoCasilla.FICHA_0 || casillas[coordenada.getX() - 1][coordenada.getY() - 1] == EstadoCasilla.FICHA_1;
    }

    public boolean isOcupada(int x, int y) {
        return casillas[x][y] == EstadoCasilla.FICHA_0 || casillas[x][y] == EstadoCasilla.FICHA_1;
    }

    public void ponerFicha(Coordenada coordenada, EstadoCasilla ficha){
        casillas[coordenada.getX() - 1][coordenada.getY() - 1] = ficha;
    }

    public boolean estaLleno(){
        int contador = 0;
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                if (isOcupada(i,j)){
                    contador++;
                }
            }
        }
        return contador == 9;
    }

    public void vaciarTablero(){
        for (int i = 0; i < casillas.length; i++) {
            for (int j = 0; j < casillas.length; j++) {
                casillas[i][j] = EstadoCasilla.VACIA;
            }
        }
    }
}
