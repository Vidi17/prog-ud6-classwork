package es.vidal.activitat18;

import java.util.Scanner;

public class Icono {

    public static Scanner teclado = new Scanner(System.in);

    private final String[][] iconos = {
            {"\uD83D\uDDE1","\uD83D\uDC80"},
            {"\uD83D\uDE01","\uD83D\uDE42"},
    };

    private int packSeleccionado;

    public Icono() {
        this.packSeleccionado = 0;
    }

    public void seleccionar(){
        System.out.printf("Vamos a seleccionar los iconos con los que jugar :\n" +
                "1)%s, %s\n" +
                "2)%s, %s\n", iconos[0][0], iconos[0][1], iconos[1][0], iconos[1][1]);
        do {
            System.out.print("Selecciona una opción: ");
            packSeleccionado = teclado.nextInt();
            if ((packSeleccionado > 2 || packSeleccionado < 1)){
                System.out.println("Error, el pack seleccionado no es válido");
            }
        }while (packSeleccionado > 2 || packSeleccionado < 1);
    }

    public String obtenerSimbolo(EstadoCasilla estadoCasilla){
        if (estadoCasilla == EstadoCasilla.FICHA_0){
            return iconos[packSeleccionado-1][0];
        }else if (estadoCasilla == EstadoCasilla.FICHA_1){
            return iconos[packSeleccionado-1][1];
        }else {
            return "--";
        }
    }

    public String obtenerSimboloJugador(EstadoCasilla estadoCasilla) {
        if (estadoCasilla == EstadoCasilla.FICHA_0) {
            return iconos[packSeleccionado-1][0];
        } else {
            return iconos[packSeleccionado-1][1];
        }
    }
}
