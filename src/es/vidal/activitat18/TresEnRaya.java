package es.vidal.activitat18;

import java.util.Scanner;

public class TresEnRaya {

    public static Scanner teclado = new Scanner(System.in);


    public static void main(String[] args) {

        System.out.println("Vamos a jugar al \"Tres en Raya\"");

        Jugador[] jugadores = new Jugador[2];

        Icono packIconos = new Icono();

        Tablero tablero = new Tablero(packIconos);

        jugadores[0] = new Jugador(EstadoCasilla.FICHA_0, packIconos);

        jugadores[1] = new Jugador(EstadoCasilla.FICHA_1, packIconos);

        Turno turno = new Turno();

        String seguirJugando;

        do {
            packIconos.seleccionar();
            juego(jugadores, tablero, turno, packIconos);
            tablero.vaciarTablero();
            System.out.print("\n¿Quieres volver a jugar (S/N)? ");
            seguirJugando = teclado.nextLine();

        }while (seguirJugando.equalsIgnoreCase("S"));

        System.out.println("Hasta la próxima");
    }

    public static void juego (Jugador[] jugadores,  Tablero tablero, Turno turno, Icono packIconos){
        do {
            jugadores[turno.getActual()].ponerFicha(tablero, packIconos);
            tablero.mostrar();
            if (tablero.hayTresEnRaya()) {
                jugadores[turno.getActual()].cantaVictoria();
                return;
            }
            turno.cambiar();
        }while (!tablero.estaLleno());
    }
}
