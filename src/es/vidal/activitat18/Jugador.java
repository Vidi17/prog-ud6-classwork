package es.vidal.activitat18;

public class Jugador {

    private final EstadoCasilla fichaJugador;
    private Icono icono;

    public Jugador(EstadoCasilla fichaJugador, Icono packIconos) {
        this.fichaJugador = fichaJugador;
        icono = packIconos;

    }

    public void ponerFicha(Tablero tablero, Icono icono){
        Coordenada coordenada = new Coordenada();
        System.out.printf("\nTira el jugador con %s\n", icono.obtenerSimbolo(fichaJugador));
        do {
            coordenada.recoger();
            if(tablero.isOcupada(coordenada)){
                System.out.println("La casilla está ocupada, por favor vuelva a introducir las coordenadas");
            }
        }while (tablero.isOcupada(coordenada));
        tablero.ponerFicha(coordenada, fichaJugador);
    }

    public void cantaVictoria(){

        System.out.println("\n Gana la partida el jugador con las fichas " + icono.obtenerSimboloJugador(fichaJugador));

    }
}