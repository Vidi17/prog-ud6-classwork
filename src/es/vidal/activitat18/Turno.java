package es.vidal.activitat18;

import java.util.Random;

public class Turno {

    public static final int TURNO_1 = 0;

    public static final int TURNO_2 = 1;

    private int numTurno;

    public Turno() {
        Random aleatorio = new Random();
        this.numTurno = aleatorio.nextInt(2);
    }

    public int getActual(){
        return numTurno;
    }

    public void cambiar(){

        if(numTurno == TURNO_1){
            numTurno = TURNO_2;
        }else {
            numTurno = TURNO_1;
        }
    }
}
