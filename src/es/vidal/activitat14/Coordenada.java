package es.vidal.activitat14;

public class Coordenada {

    private int x;

    private int y;

    public Coordenada(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getDistancia(Coordenada coordenada2){
        return (int) Math.sqrt((Math.pow((coordenada2.x - x), 2))+(Math.pow((coordenada2.y - y), 2)));
    }

    public String toString (){
       return "(" + x + "," + y + ")";
    }
}
