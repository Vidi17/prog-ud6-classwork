package es.vidal.activitat14;

public class TestCoordenada {
    public static void main(String[] args) {
        Coordenada punto1 = new Coordenada(0,0);
        Coordenada punto2 = new Coordenada(5,3);
        Coordenada punto3 = new Coordenada(2,-1);
        Coordenada punto4 = new Coordenada((punto2.getX() + punto3.getX())/2,(punto2.getY() + punto3.getY())/2);
        Coordenada punto5 = new Coordenada(4,3);

        mostrarDistancia(punto1.toString(), punto5.toString(),punto1.getDistancia(punto5));

    }
    public static void mostrarDistancia (String coordenada1 ,String coordenada2, int distancia){
        System.out.printf("La distancia entre el punto %s y el punto %s es de %d\n",coordenada1, coordenada2, distancia);
    }
}
