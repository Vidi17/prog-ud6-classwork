package es.vidal.Activitat13;

public class TestFecha {
    public static void main(String[] args) {
        Fecha fecha1 = new Fecha(16,1,2021);
        Fecha fecha1Copia = new Fecha(fecha1);
        Fecha fecha2 = new Fecha("16/1/2021");
        Fecha fecha3 = new Fecha();

        System.out.println("=== START - Prueba de Constructores - START ===\n" +
                "--- Creo un nuevo objeto utilizando el constructor parametrizado int int int(16, 1, 2021)\n" +
                "---");
        mostrarInfoFecha(fecha1);
        System.out.println("--- Creo un nuevo objeto utilizando la fecha anterior mediante el constructor por copia\n" +
                "---");
        mostrarInfoFecha(fecha1Copia);
        System.out.printf("--- Utilizando el método equals, compruebo que las fechas representadas por ambos\n " +
                "objetos son iguales ---\n" +
                "La fecha creada con los constructores anteriores son iguales: %s\n" +
                "-------------------------------\n\n" +
                "--- Creo un objeto utilizando el constructor String 16/1/2021 ---\n", fecha1.isEqual(fecha1Copia));
        mostrarInfoFecha(fecha2);
        System.out.println("--- Creo un objeto utilizando el constructor por defecto ---");
        mostrarInfoFecha(fecha3);
        System.out.println("=== FIN - Prueba de Constructores - FIN ===\n\n" +
                "=== START - Prueba de Métodos anyadir/restar dias - START ===\n\n" +
                "--- Día siguiente a la fecha inicial (16-1-2021) - (+1 día) ---");
        Fecha fecha4 = fecha1.anyadir(1);
        mostrarInfoFecha(fecha4);
        System.out.println("--- Día anterior a la fecha inicial (16-1-2021) - (-1 día) ---");
        Fecha fecha5 = fecha1.restar(1);
        mostrarInfoFecha(fecha5);
        System.out.println("--- Fecha correspondiente a restar 30 días a la fecha inicial (16-1-2021) - (-30 dias) ---");
        Fecha fecha6 = fecha1.restar(30);
        mostrarInfoFecha(fecha6);
        System.out.println("=== FIN . Prueba de Métodos anyadir/restar - FIN ===\n\n" +
                "=== START - Prueba del método modificador - START ===\n\n" +
                "--- Modifico la fecha del primer objeto creado (16-1-2020) por la fecha 22-1-2021 ---");
        fecha1.set(22,1,2021);
        mostrarInfoFecha(fecha1);
        System.out.println("=== FIN - Prueba del método modificador - FIN ===");

    }
    public static void mostrarInfoFecha(Fecha fecha){
        fecha.mostrarFormatoES(fecha.getDia(), fecha.getMes(), fecha.getAnyo());
        System.out.println();
        fecha.mostrarFormatoGB(fecha.getDia(), fecha.getMes(), fecha.getAnyo());
        System.out.println();
        fecha.mostrarFormatoTexto(fecha.getDia(), fecha.getMes(), fecha.getAnyo());
        System.out.println();
        System.out.printf("La fecha es correcta: %s\n", fecha.isCorrecta(fecha.getDia(), fecha.getMes(), fecha.getAnyo()));
        System.out.printf("La fecha es festivo: %s\n", fecha.isFestivo(fecha.getDia(), fecha.getMes(), fecha.getAnyo()));
        System.out.printf("El día de la semana es: %s\n", fecha.getDiaSemana(fecha.getDia(), fecha.getMes(), fecha.getAnyo()));
        System.out.println("-------------------------------\n");
    }
}

